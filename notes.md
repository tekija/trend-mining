2.

Conference Papers and Articles (Scopus DB)

Journals: ICSM, ICSME, SMERP, CSMR, SANER


Software AND (Evolution OR Maintenance OR Correct* OR Adapt* OR Perfect* OR Prevent* OR Re-construct OR Re-structure OR Modify OR Updat*)

Can try without wildcards as well:
Software AND (Evolution OR Maintenance OR Corrective OR Adaptive OR Perfective OR Preventive OR Re-construct OR Re-structure OR Modify OR Update)

3.

Will need to see the initial results to see if there should be stopwords.